const assert = require('assert');
const { checkEnableTime } = require("../jobPosting")
describe('jobPosting'),function(){
    describe('checkEnableTime()'),function(){ 
        it('should return true when เวลาอยู่ระหว่าง เริ่มต้นและสิ้นสุด', function () {
            const startTime = new Date(2021,10,2)
            const endTime = new Date(2021,10,10)
            const today = new Date(2021,10,3)
            const expectedResult=true;

            const actualResult = checkEnableTime(startTime,endTime,today)
            assert.strictEqual(actualResult, expectedResult)
        })
        it('should return true when เวลา=เริ่มต้น', function () {
            const startTime = new Date(2021,10,2)
            const endTime = new Date(2021,10,10)
            const today = new Date(2021,10,2)
            const expectedResult=true;

            const actualResult = checkEnableTime(startTime,endTime,today)
            assert.strictEqual(actualResult, expectedResult)
        })
        it('should return true when เวลา=สิ้นสุด', function () {
            const startTime = new Date(2021,10,2)
            const endTime = new Date(2021,10,10)
            const today = new Date(2021,10,10)
            const expectedResult=true;

            const actualResult = checkEnableTime(startTime,endTime,today)
            assert.strictEqual(actualResult, expectedResult)
        })
        it('should return false when เวลา>สิ้นสุด', function () {
            const startTime = new Date(2021,10,2)
            const endTime = new Date(2021,10,10)
            const today = new Date(2021,10,11)
            const expectedResult=false;

            const actualResult = checkEnableTime(startTime,endTime,today)
            assert.strictEqual(actualResult, expectedResult)
        })
        it('should return false when เวลา<เริ่มต้น', function () {
            const startTime = new Date(2021,10,2)
            const endTime = new Date(2021,10,10)
            const today = new Date(2021,10,1)
            const expectedResult=false;

            const actualResult = checkEnableTime(startTime,endTime,today)
            assert.strictEqual(actualResult, expectedResult)
        })
        
    }
}


// const startTime = new Date(2021,10,2)
// const endTime = new Date(2021,10,10)
// const today = new Date(2021,10,3)
// const expectedResult=true;

// const actualResult = checkEnableTime(startTime,endTime,today)

// if(actualResult === expectedResult){
//     console.log('เวลาอยู่ระหว่าง เริ่มต้นและสิ้นสุด')
// }else{
//     console.log('เวลาไม่อยู่ระหว่าง เริ่มต้นและสิ้นสุด')
// }